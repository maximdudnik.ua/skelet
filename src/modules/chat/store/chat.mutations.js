import types from './chat.mutationTypes';

export default {
  [types.CHANGE_CHAT_NAME](state, name) {
    state.chat_name = name;
  },
};
