import Vue from 'vue';
import Vuex from 'vuex';

// shared store module
import profile from 'shared/storeModules/profile';
import permissions from './permissions';

Vue.use(Vuex);

/* eslint-disable no-new */
const store = new Vuex.Store({
  modules: {
    permissions,
    profile,
  },
});

export default store;
