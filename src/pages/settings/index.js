import routes from './settings.page.routes';

export default {
  install(Vue, options = {}) {
    if (!options.router) {
      throw Error('Router not provided');
    }

    options.router.addRoutes(routes);
  },
};
