import getters from './chat.getters';
import mutations from './chat.mutations';
import actions from './chat.actions';

export default {
  namespaced: true,
  state: {
    chat_name: 'Current chat name',
  },
  getters,
  mutations,
  actions,
};
