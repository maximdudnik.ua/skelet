# skelet

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Structure

```
├─ shared/ < -- Shared components, modules, plugins, storeModules etc (npm module)
.......
├─ src/
│  ├─ App.vue
│  ├─ main.js
│  ├─ api/ <-- common api
│  ├─ plugins/
│  ├─ services/
│  ├─ components/
│  ├─ store/
│  │   ├─ auth/
│  │   └─ profile/
│  │      ├─ profile.action.js
│  │      └─ profile.getters.js
│  │      └─ profile.mutationTypes.js
│  │      └─ profile.state.js
│  │      └─ index.js
│  ├─ pages/
│  │  ├─ settings/
│  │  │  ├─ subpages/
│  │  │  │  ├─ SettingsProfilePage.vue
│  │  │  │  └─ SettingsSystemPage.vue
│  │  │  ├─ index.js
│  │  │  ├─ settings.page.routes.js
│  │  │  └─ SettingsPage.vue   < -- Container for components and modules, access to core store, api, services etc
│  │  ├─ chat/
│  │  └─ settings/
│  ├─ modules/ < -- Individual pieces with own store, api, socket-connection, components,
│  │  ├─ chat/
│  │  │  ├─ index.js
│  │  │  ├─ api/
│  │  │  ├─ components/
│  │  │  ├─ services/
│  │  │  │  └─ transformData.js
│  │  │  ├─ store/
│  │  │  │  ├─ chat.action.js
│  │  │  │  └─ chat.getters.js
│  │  │  │  └─ chat.mutationTypes.js
│  │  │  │  └─ chat.state.js
│  │  │  │  └─ index.js
│  │  │  └─ Chat.vue
```
