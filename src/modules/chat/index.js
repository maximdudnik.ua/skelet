import Component from './Chat.vue';
import store from './store';

export default {
  install(Vue, options = {}) {
    if (!options.store) {
      throw Error('Store not provided');
    }

    Vue.component('chat', Component);
    options.store.registerModule('chat', store);
  },
};
