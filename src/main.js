import CallModule from 'shared/modules/call';
import VuePhoenix from 'shared/plugins/phoenix';

import Vue from 'vue';
import App from './App.vue';
// import VueSocketIO from 'vue-socket.io'

import './styles/quasar.styl';
import '@quasar/extras/material-icons/material-icons.css';

import {
  Quasar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QPage,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QCard,
  QCardActions,
  Dialog,
  QDialog,
  Notify,
} from 'quasar';

import store from './store';

import router from './router';


import ChatModule from '@/modules/chat';

import SettingsPage from '@/pages/settings'; // can be moved to shared


Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QCard,
    QCardActions,
    QDialog,
  },
  directives: {},
  plugins: {
    Dialog,
    Notify,
  },
});

Vue.config.productionTip = false;

Vue.use(CallModule, { store });
Vue.use(ChatModule, { store });

Vue.use(SettingsPage, { router });

// https://vip30.github.io/vue-phoenix/ --- test client
Vue.use(new VuePhoenix({
  connection: 'wss://vue-phoenix.herokuapp.com/socket',
  debug: true,
  options: {
    // heartbeatIntervalMs: 5000,
  },
}), {});

// Vue.use(new VueSocketIO({
//   debug: true,
//   connection: 'http://localhost:5000',
// })); // vortex);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
