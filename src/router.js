import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/chat',
      name: 'chat',
      props: {
        room: 'Room1',
        isActive: true,
      },
      component: () => import('./modules/chat/Chat.vue'), // Load module as page
    },
    {
      path: '/call',
      name: 'call',
      component: () => import('./pages/call/CallPage.vue'), // Page contains modules and components
    },
  ],
});
