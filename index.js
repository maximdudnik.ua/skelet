import { Socket } from './phoenix';

const socket = new Socket('wss://director.telemed.care:4000');
socket.connect();

const channel = socket.channel('lobby', {});
channel.join()
  .receive('ok', (resp) => { console.log('Joined successfully', resp); })
  .receive('error', (resp) => { console.log('Unable to join', resp); });
