import types from './chat.mutationTypes';
import api from '../api';

export default {
  init: () => {},
  changeChatName: async ({ commit }, query) => {
    const res = await api.getChatName(query);

    return commit(types.CHANGE_CHAT_NAME, res.data.name);
  },
};
