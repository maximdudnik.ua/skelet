export default class VuePhoenixListener {
  constructor(socket, emitter) {
    this.socket = socket;
    this.register();
    this.emitter = emitter;
  }

  /**
   * Listening all phoenix events
   */
  register() {
    this.socket.onMessage((packet) => {
      this.onEvent(`${packet.topic}-${packet.event}`, packet);
    });
  }

  /**
   * Broadcast all events to vuejs environment
   */
  onEvent(event, args) {
    this.emitter.emit(event, args);
  }

}
