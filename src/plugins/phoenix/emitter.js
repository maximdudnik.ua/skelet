import Logger from './logger';

export default class EventEmitter {

  constructor() {
    this.listeners = new Map();
  }

  /**
   * register new event listener with vuejs component instance
   * @param event
   * @param callback
   * @param component
   */
  addListener(event, callback, component) {
    if (typeof callback === 'function') {
      if (!this.listeners.has(event)) this.listeners.set(event, []);
      this.listeners.get(event).push({callback, component});

      Logger.info(`#${event} subscribe, component: ${component.$options.name}`);
    } else {
      throw new Error(`callback must be a function`);
    }
  }

  /**
   * remove a listenler
   * @param event
   * @param component
   */
  removeListener(event, component) {
    if (this.listeners.has(event)) {
      const listeners = this.listeners.get(event).filter(listener => (
        listener.component !== component
      ));

      if (listeners.length > 0) {
        this.listeners.set(event, listeners);
      } else {
        this.listeners.delete(event);
      }

      Logger.info(`#${event} unsubscribe, component: ${component.$options.name}`);
    }
  }

  /**
   * broadcast incoming event to components
   * @param event
   * @param args
   */
  emit(event, args) {
    if (this.listeners.has(event)) {
      Logger.info(`Broadcasting: #${event}, Data:`, args);

      this.listeners.get(event).forEach((listener) => {
        listener.callback.call(listener.component, args);
      });
    }
  }
}
