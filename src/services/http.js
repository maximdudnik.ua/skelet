import axios from 'axios';

// https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter
function updateQueryString(key, value, url) {
  let re = new RegExp('([?&])' + key + '=.*?(&|#|$)(.*)', 'gi');
  let hash;

  if (re.test(url)) {
    if (typeof value !== 'undefined' && value !== null) return url.replace(re, '$1' + key + '=' + value + '$2$3');
    else {
      hash = url.split('#');
      url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
      if (typeof hash[1] !== 'undefined' && hash[1] !== null) url += '#' + hash[1];
      return url;
    }
  } else {
    if (typeof value !== 'undefined' && value !== null) {
      let separator = url.indexOf('?') !== -1 ? '&' : '?';
      hash = url.split('#');
      url = hash[0] + separator + key + '=' + value;
      if (typeof hash[1] !== 'undefined' && hash[1] !== null) url += '#' + hash[1];
      return url;
    }
    return url;
  }
}

export default ({url, method = 'GET', data, timeout = 60000, headers}) => {
  let resURL = updateQueryString('lang', 'ua', url);

  return axios({url: resURL, method, data, timeout, headers}).catch(err => err.response);
};
