export default {
  /**
   * Join all channels and register events
   */
  mounted() {
    if (this.$options.channels) {
      Object.keys(this.$options.channels).forEach((topic) => {
        const channel = this.$vuePhoenix.joinChannel(topic);

        const joinCb = this.$options.channels[topic].join;
        if (typeof joinCb === 'function') {
          channel.receive('ok', () => {
            joinCb.apply(this);
          });
        }

        Object.keys(this.$options.channels[topic]).forEach((event) => {
          this.$vuePhoenix.emitter.addListener(`${topic}-${event}`, this.$options.channels[topic][event], this);
        });
      });
    }
  },

  /**
   * Leave channels
   */
  beforeDestroy() {
    if (this.$options.channels) {
      Object.keys(this.$options.channels).forEach((topic) => {

        this.$vuePhoenix.leaveChannel(topic);

        Object.keys(this.$options.channels[topic]).forEach((event) => {
          this.$vuePhoenix.emitter.removeListener(`${topic}-${event}`, this);
        });

        this.$vuePhoenix.emitter.removeListener(`${topic}-join`, this);
        this.$vuePhoenix.emitter.removeListener(`${topic}-close`, this);
        this.$vuePhoenix.emitter.removeListener(`${topic}-error`, this);
      });
    }
  },
};
